<?php

interface ReaderInterface {

public function read(string $input): OfferCollectionInterface;
}

interface OfferInterface {}

interface OfferCollectionInterface {
    public function get(int $index): OfferInterface;
    public function getIterator(): Iterator;
    public function range($key,$from,$to): array;
    public function filterByKey($key,$value): array;
    public function setCollection($collection): void;
}

class Offer implements OfferInterface{
    public int $offerId;
    public string $productTitle;
    public int $vendorId;
    public float $price;

}

class Reader implements ReaderInterface{
    public function read(string $input): OfferCollectionInterface{
        $offersCollection = new OfferCollection();
        try{
            $data = json_decode($input);
            $foffers = array();
            foreach($data as $offer){
                $new_offer= new Offer();
                foreach($offer as $key => $value){
                    $new_offer->{$key} = $value;
                }
                array_push($foffers,$new_offer);
            }
            $offersCollection->setCollection($foffers);
        }catch(Exception $e){
            $offersCollection->setCollection(array());
        }
        return $offersCollection;

    }
}

class OfferCollection implements OfferCollectionInterface{
    private $collection = null;
    public function get(int $index): OfferInterface{
        if($collection != null && count($collection) >= $index) return $this->collection[$index];
        return null;
    }
    public function getIterator(): Iterator{
        if($collection != null && count($collection) >= $index) return ArrayObject($this->collection).getIterator();
        return null;
    } 
    public function range($key,$from,$to): array{
        return array_filter($this->collection,function($item) use($key,$from,$to) { return $item->{$key} >= $from && $item->{$key} <= $to;});
    }
    public function filterByKey($key,$value): array{
        return array_filter($this->collection,function($item) use($key,$value) { return $item->{$key} == $value; });
    }  
    public function setCollection($collection): void{
        $this->collection = $collection;

    }

}


class App{
    public function validParams($params):bool{
        if(count($params) < 3 ){
            echo "pelase define an action and some parameters";
            return false;

        }else if(strpos($params[1],"count_by_") !== false){
            return true;
        }
        return false;
    }

    public function execute($params,$url):int{
        if(!$this->validParams($params)) return 0;
        $request = curl_init();
        curl_setopt($request,CURLOPT_URL,$url);
        curl_setopt($request,CURLOPT_RETURNTRANSFER,1);
        $data = curl_exec($request);
        $reader = new Reader();
        $collection = $reader->read($data);
        if(strpos($params[1],"count_by_") !== false){
            if(strpos($params[1],"_range") !== false){
                return count($collection->range($this->getKey($params[1]),$params[2],$params[3]));
            }else{
                return count($collection->filterByKey($this->getKey($params[1]),$params[2]));
            }
        }else echo("function not defined");
    }
   

    public function getKey($param):string{
        $key = str_replace("_","",ucwords(str_replace("_range","",str_replace("count_by_","",$param)),"_"));
        $key[0] = strtolower($key[0]);
        return $key;
    }
}